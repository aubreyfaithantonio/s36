const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error) {
			console.log(error)
			return error
		}

		return savedTask
	})
}

module.exports.getAllTask = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}

		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {
	return Task.findByIdAndDelete(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}
		return result.name + " is successfully deleted"
	})
}

module.exports.getSpecificTask = (task_id) => {
	return Task.findById(task_id).then((specificTask, error) => {
		if(error){
			console.log(error)
			return error
		}

		return specificTask
	})
}

module.exports.changeToComplete = (task_id) => {
	return Task.findById(task_id).then((task, error) => {
		if(error){
			console.log(error)
			return error
		}
		if(task == null){
			return "Task not found"
		}
		task.status = "Complete"
		return task.save().then((completedTask, error) => {
			if(error){
				console.log(error)
				return error
			}
			return completedTask
		})
	})
}


