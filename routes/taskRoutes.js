const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTask().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

// Delete Task
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// Get a specific task
router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then((result) => {
		response.send(result)
	})
})

// Change status of a task
router.patch('/:id/complete', (request, response) => {
	TaskController.changeToComplete(request.params.id).then((result) => {
		response.send(result)
	})
})
module.exports = router